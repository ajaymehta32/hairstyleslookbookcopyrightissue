package ajaymehta.hairstyleslookbook.util;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import ajaymehta.hairstyleslookbook.model.Hairstyle;

/**
 * Created by Ajay Mehta on 7/29/2017.
 */

// Utils contain methods required to parse seed json file and also populate the model Profile.java

public class Utils {

    // return string arraylist that contains object ...we gonna call  this method ..it will call next loadJsonFromAsset
    // for more info see .our projec ..JsonFromAssetGsonGlide..

    private static final String TAG = "Utils";

    public static List<Hairstyle> loadStyles(Context context , String filename){
        try{
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            JSONArray array = new JSONArray(loadJSONFromAsset(context, filename));
            List<Hairstyle> profileList = new ArrayList<>();
            for(int i=0;i<array.length();i++){
                Hairstyle hairstyle = gson.fromJson(array.getString(i), Hairstyle.class);
                profileList.add(hairstyle);
            }
            return profileList;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    private static String loadJSONFromAsset(Context context, String jsonFileName) {
        String json = null;
        InputStream is=null;
        try {
            AssetManager manager = context.getAssets();
            Log.d(TAG,"path "+jsonFileName);
            is = manager.open(jsonFileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    // to gives point of all screen  .so accroding to that our card view fits in all phones..

    public static Point getDisplaySize(WindowManager windowManager){
        try {
            if(Build.VERSION.SDK_INT > 16) {
                Display display = windowManager.getDefaultDisplay();
                DisplayMetrics displayMetrics = new DisplayMetrics();
                display.getMetrics(displayMetrics);
                return new Point(displayMetrics.widthPixels, displayMetrics.heightPixels);
            }else{
                return new Point(0, 0);
            }
        }catch (Exception e){
            e.printStackTrace();
            return new Point(0, 0);
        }
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }
}