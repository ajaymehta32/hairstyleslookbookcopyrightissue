package ajaymehta.hairstyleslookbook.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Avi Hacker on 8/5/2017.
 */

public class Hairstyle {

    @SerializedName("name")    // if you forgot to wriite SerializedName("url) <-- its not gonna have images n u not gonna get it ..but still i was getting ..name ...Strange...
    @Expose
    private String name;

    @SerializedName("url")
    @Expose
    private String imageUrl;

    public Hairstyle(String name, String imageUrl) {
        this.name = name;
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
