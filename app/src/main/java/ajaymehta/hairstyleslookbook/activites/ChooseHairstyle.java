package ajaymehta.hairstyleslookbook.activites;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import ajaymehta.hairstyleslookbook.R;

public class ChooseHairstyle extends AppCompatActivity implements View.OnClickListener {

    TextView menHairstyle, womenHairstyle, kidsHairstyles, tvHeading;
    public static String key ="WhomStyle";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_hairstyle);

        menHairstyle = (TextView) findViewById(R.id.tv_men_hairstyle);
        womenHairstyle = (TextView) findViewById(R.id.tv_women_hairstyle);
        kidsHairstyles = (TextView) findViewById(R.id.tv_kids_hairstyle);
        tvHeading = (TextView) findViewById(R.id.tv_heading);

//====================================================
        Typeface  myCustomFont= Typeface.createFromAsset(getAssets(), "fonts/cal.otf");

        tvHeading.setTypeface(myCustomFont);


        menHairstyle.setOnClickListener(this);
        womenHairstyle.setOnClickListener(this);
        kidsHairstyles.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {

        Intent i= null;


        switch(v.getId()){

            case R.id.tv_men_hairstyle:
                i = new Intent(ChooseHairstyle.this, HairstylesList.class);
                i.putExtra(key,1);
                startActivity(i);
                break;

            case R.id.tv_kids_hairstyle:
                i = new Intent(ChooseHairstyle.this, HairstylesList.class);
                i.putExtra(key,2);
                startActivity(i);
                break;

            case R.id.tv_women_hairstyle:
                i = new Intent(ChooseHairstyle.this, HairstylesList.class);
                i.putExtra(key,3);
                startActivity(i);
                break;
        }


    }
}
