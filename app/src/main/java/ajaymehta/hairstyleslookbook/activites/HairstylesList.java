package ajaymehta.hairstyleslookbook.activites;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;


import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import ajaymehta.hairstyleslookbook.R;
import ajaymehta.hairstyleslookbook.adapter.HairStylesListAdapter;

public class HairstylesList extends AppCompatActivity {

    private AdView mAdView;
    InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hairstyles);
 // it takes a bit of time to load ads ..so i am write is sabse phele


        // show instatital ad
        showInstatialAd();



        // calling adview method from onCreate

        showBannerAd();



        int intentValue = getIntent().getIntExtra(ChooseHairstyle.key, -1);

        if(intentValue ==1) {
            getSupportActionBar().setTitle("Men HairStyles");

        }

        if(intentValue ==2) {
            getSupportActionBar().setTitle("Teen Boys HairStyles");
        }

        if(intentValue ==3) {
            getSupportActionBar().setTitle("Women HairStyles");
        }


        ListView hairstyles = (ListView)findViewById(R.id.lv_hairstyles);

        HairStylesListAdapter adapter = new HairStylesListAdapter(getLayoutInflater(), this , intentValue);

        hairstyles.setAdapter(adapter);




    }  //end of onCreate method .....

    private void showInstatialAd() {

        mInterstitialAd = new InterstitialAd(this);

        // set the ad unit ID
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_full_screen));

        AdRequest adRequest = new AdRequest.Builder()
                .build();

        mInterstitialAd.loadAd(adRequest);

        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                showInterstitial();
             //   Toast.makeText(getApplicationContext(), "Ad loaded successfully!", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onAdClosed() {
                //  Toast.makeText(getApplicationContext(), "Ad is closed!", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                //   Toast.makeText(getApplicationContext(), "Ad failed to load! error code: " + errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
                //   Toast.makeText(getApplicationContext(), "Ad left application!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
              //  Toast.makeText(getApplicationContext(), "Ad is opened!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }




    }


    //==========================================================
    private void showBannerAd() {


        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdClosed() {
              //  Toast.makeText(getApplicationContext(), "Ad is closed!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
              //  Toast.makeText(getApplicationContext(), "Ad failed to load! error code: " + errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdLeftApplication() {
             //   Toast.makeText(getApplicationContext(), "Ad left application!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }
        });

        mAdView.loadAd(adRequest);


    }   // end of showAd method ...

    @Override
    public void onPause() {               // pasue adview with life cycle.
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {           // resume adview
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {               // destory adview
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }



    }

//======================concept of app==============================

// what is the concept behind this project ... it is basically about objects ..... see we not giving birth to objects ..but they are already made
// n coming from json file ...now to use them we have to  set the details of every object coming ..so that we could get them n use them
// to store the detais of every object ...we created a HairStyle file in model package ....that will initlize (birth) to every object ...n we gonna save that object in array list ...
// hey but .all of this work is getting done by gson ..in utility class ..that is taking object coming from json file ...n its setting them ..in constructor n then putting them in arraylsit
//one by one ...see when you see a screen ...you can see  if they are so many things that are same ..means there are so many object with same details ..

