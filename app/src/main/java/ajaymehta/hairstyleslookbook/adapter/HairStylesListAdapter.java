package ajaymehta.hairstyleslookbook.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

import ajaymehta.hairstyleslookbook.R;
import ajaymehta.hairstyleslookbook.model.Hairstyle;
import ajaymehta.hairstyleslookbook.util.Utils;


/**
 * Created by Ajay Mehta on 8/4/2017.
 */


public class HairStylesListAdapter extends BaseAdapter {

    LayoutInflater mInflater;
    Context mContext;
    List<Hairstyle> hairstyleList;
    int intentValue;


    public HairStylesListAdapter(LayoutInflater inflater, Context context , int value){
        mInflater = inflater;
        mContext = context;
        intentValue = value;

        check();


    }

    private void check() {
        if(intentValue == 1) {
            hairstyleList = Utils.loadStyles(mContext , "menHairstyles.json");  // got the list of objects of type Hairstyle contains (name and url)
        } else if (intentValue == 2) {

            hairstyleList = Utils.loadStyles(mContext , "kidsHairstyles.json");  // got the list of objects of type Hairstyle contains (name and url)

        } else {
            hairstyleList = Utils.loadStyles(mContext , "womenHairstyles.json");  // got the list of objects of type Hairstyle contains (name and url)


        }
    }


    @Override
    public int getCount() {
        return hairstyleList.size();
    }

    @Override
    public Object getItem(int position) {

        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null) {


            convertView = mInflater.inflate(R.layout.hairstyles_adapter, parent, false);
        }


        ImageView hairstylePic = (ImageView) convertView.findViewById(R.id.iv_hairstyle_pic);

        TextView hairstyleName = (TextView) convertView.findViewById(R.id.tv_hairstyle_name);

        for(int i=0; i<hairstyleList.size(); i++) {

        //    Glide.with(mContext).load(hairstyleList.get(position).getImageUrl()).into(hairstylePic);

            /* I have used picasso instead of glide because it provides resize method ..it resizes the image exactly the
            size of imageview ..so why width,height -> resize(780,800) .. hey they provide quality hd pics
            if you take reszie(200,200) image gonna be same as imageview but with low quality ... so i guess setting 800 gives the hd qualtiy ..so i took it ..

            but you can do trial n error .... to make it high quality n set perfectly (not streatched or shrunk ..)


            * */

            Picasso.with(mContext).load(hairstyleList.get(position).getImageUrl()).resize(780,800).into(hairstylePic);

            hairstyleName.setText(hairstyleList.get(position).getName());
        }


        return  convertView;
    }
}